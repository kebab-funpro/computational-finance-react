import React from 'react';
import { useState, useEffect } from 'react';
import { reduxForm, Field, reducer as formReducer } from 'redux-form';
import { Provider } from 'react-redux';
import { createStore, combineReducers } from 'redux';
import $ from 'jquery';
import withFooter from '../util/footer';

const rootReducer = combineReducers({
    form: formReducer,
  });

const store = createStore(rootReducer);

const renderField = ({ input, label, type, meta: { touched, error, warning } }) => (
    <div>
        <div className="control">
        <label className="field">{label}</label>
            <div className="input-container">
                <input className="input" {...input} type={type}/>
                {touched && ((error && <span style={{float:'left'}}>{error}</span>) || (warning && <span>{warning}</span>))}
            </div>
        </div>

        <style jsx>{`
        
            label {
                margin-bottom: 1vh;
            }

            label, input {
                display: block;
            }

            .input-container {
                margin-bottom: 4vh;
            }

            input {
                font-family: Muli;
                height: 5vh;
                width: 18vw;
                border: 0;
                border-radius: 10px;
                font-size: 1em;
                padding: 2px;
            }

            span {
                font-size: 0.5em;
                color: red;
                margin-top: 1vh;
            }

        `}
        </style>
    </div>
);

const renderWagesField = ({ input, type, meta: { touched, error, warning } }) => (
    <div className="container">
        <div className="control">
            <div className="input-container">
                <input className="input wages" {...input} type={type}/>
                {touched && ((error && <span style={{float:'left'}}>{error}</span>) || (warning && <span>{warning}</span>))}
            </div>
        </div>

        <style jsx>{`

            .container {
                display: inline-block;
                width: 20%;
            }

            .input-container {
                margin-bottom: 4vh;
            }

            input {
                font-family: Muli;
                height: 5vh;
                width: 100%;
                display: inline-block;
                border: 0;
                border-radius: 10px;
                font-size: 1em;
                padding: 2px;
            }

            span {
                font-size: 0.5em;
                color: red;
                margin-top: 1vh;
            }

        `}    
        </style>
    </div>
);

function validate_curry(type) {
    return (value) => {
        return type === "integer" ? value != '' && value >= 1 : value != '' && value <= 1;
    }
}

const validateInteger = validate_curry("integer");
const validateDecimal = validate_curry("decimal");

const validate = val => {
    const errors = {};
    if (!validateInteger(val.unemploymentbenefit)) {
        errors.unemploymentbenefit = 'Input is not valid';
    }
    if (!validateInteger(val.leisure)) {
        errors.leisure = 'Input is not valid';
    }
    if (!validateDecimal(val.probabilityofgettinghired)) {
        errors.probabilityofgettinghired = 'Input is not valid';
    }
    if (!validateDecimal(val.probabilityofgettingfired)) {
        errors.probabilityofgettingfired = 'Input is not valid';
    }
    if (!validateDecimal(val.weeklydiscountrate)) {
        errors.weeklydiscountrate = 'Input is not valid';
    }
    if (!validateInteger(val.wagefrom)) {
        errors.wagefrom = 'Input is not valid';
    }
    if (!validateInteger(val.wageto)) {
        errors.wageto = 'Input is not valid';
    }
    return errors;
};

let InputForm = props => {
    const { handleSubmit } = props;
    return (
        <form onSubmit={handleSubmit}>
            <ul>
                <li>
                    <Field name="unemploymentbenefit" component={renderField} type="number" label="Unemployment Benefit (u)"/>
                </li>
                <li>
                    <Field name="leisure" component={renderField} type="number" label="Leisure (v)"/>
                </li>
                <li>
                    <Field name="probabilityofgettinghired" component={renderField} type="number" label="Probability of Getting Hired (p)"/>
                </li>
                <li>
                    <Field name="probabilityofgettingfired" component={renderField} type="number" label="Probability of Getting Fired (q)"/>
                </li>
                <li>
                    <Field name="weeklydiscountrate" component={renderField} type="number" label="Weekly Discount Rate"/>
                </li>
                <li>
                    <label>Wage (w)</label>
                    <Field name="wagefrom" component={renderWagesField} type="number"/>
                    <p className="wage-to">to</p>
                    <Field name="wageto" component={renderWagesField} type="number" />
                </li>
            </ul>
            <div className="field">
            <div className="control">
                <button className="submit-button">SUBMIT</button>
            </div>
            </div>
            <style jsx> {`

                form {
                    font-family: Muli;
                    font-size: 1.6em;
                    position: absolute;
                    top: 16vw;
                    left: 50vw;
                }

                label {
                    display: block;
                    margin-bottom: 1vh;
                }
                
                ul {
                    width: 100%;
                }

                ul li {
                    width: 49%;
                    display: inline-block;
                }

                p {
                    margin: 0;
                }

                .wage-to {
                    display: inline-block;
                    margin-left: 1vw;
                    margin-right: 1vw;
                    font-family: Muli;
                    font-size: 1em;
                }

                .submit-button {
                    width: 250px;
                    height: 90px;
                    background: #FAD259;
                    box-shadow: 0px 4px 4px rgba(0,0,0,0.25);
                    border-radius: 50px;
                    border: 0;
                    font-family: 'Muli',sans-serif;
                    font-style: normal;
                    font-weight: bold;
                    font-size: 30px;
                    line-height: 45px;
                    text-align: center;
                    color: #333752;
                    cursor: pointer;
                    margin-left: 35%;
                }

            `}
            </style>
        </form>
    );
};

InputForm = reduxForm({
    form: 'jobSearch',
    validate,
})(InputForm);

function InputPage(props) {
    const [resultIsOut, setResultIsOut] = useState(false);

    useEffect(() => {
        $('body').css({
            overflowX: 'hidden',
            margin: 0
        });
    })

    useEffect(() => {
        if(resultIsOut) {
            $('html,body').animate({
                scrollTop: $(".result-page").offset().top},
                'slow');
        }
    })  

    function removeFooter() {
        const footer = document.querySelector('.footer-container');
        footer.style.display = 'none';
    }

    function addCell(index, val) {
        var dict = {1: "Active", 2: "Inactive"}
        var val = (index == 0 ? val : dict[val]);
        return val;
    }

    function handleJobSearch(values) {
		Object.keys(values).forEach(key => {
		    values[key] = parseFloat(values[key])
        })

        fetch('https://kebab-api-haskell.herokuapp.com/calculate', {
                method: 'POST',
                body: JSON.stringify(values), // Made by Ridwan 1606886974
            })
            .then((response) => {
                response.json().then((data) => {
                    var table = document.getElementById('table');
                    var tbody = table.getElementsByTagName('tbody')[0];
                    if(tbody.hasChildNodes()) {
                        while(tbody.firstChild) {
                            tbody.firstChild.remove()
                        }
                    }
                    var tableRef = tbody;
                    data.map((dato, rowIdx) => {
                        var row = tableRef.insertRow();
                        dato.map((val, index) => {
                            var cell = row.insertCell();
                            Object.assign(cell.style,{
                                textAlign: 'center',
                                border: '5px solid #FFFFFF',
                                width: '20vw',
                                height: '5vh',
                                fontFamily: 'Muli',
                                fontStyle: 'normal',
                                fontSize: '28px',
                                lineHeight: '35px'
                            });
                            
                            if(rowIdx == data.length -1) cell.style.borderBottom = 0;

                            if(index == 0) cell.style.borderLeft = 0;

                            if(index == 2) cell.style.borderRight = 0;

                            cell.appendChild(document.createTextNode(addCell(index, val)));
                            
                        })
                    })
                });
            })
        setResultIsOut(true);
        removeFooter();
    };
          
    return (
        <Provider store = {store}>
            <div className="input-page-container">
                <div className="bg-top-white-wave">
                    <img src="../static/image/top_white_wave.svg" />
                    <div className="title-input">
                        Input Your Data
                    </div>
                </div>

                <div className="man-sitting-container">
                    <img src="../static/image/man_sitting.png" />
                </div>

                <InputForm onSubmit={handleJobSearch} />
                
                {/* { resultIsOut ?  */}
                <div className="result-page" style={ resultIsOut ? {display: 'block'} : {display: 'none'}}>
                    <h1>Your Result</h1>
                    <h2>Optimal Labor Participation Rule</h2>
                    <table id="table">
                        <thead>
                            <tr>
                                <th>Wage</th>
                                <th>Unemployed</th>
                                <th>Employed</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                {/* : null } */}
                <style jsx> {`
                @import url('https://fonts.googleapis.com/css?family=Muli&display=swap');

                p, h1, h2, h3 {
                    font-family: Muli;
                    font-style: normal;
                }

                h2 {
                    text-align: center;
                }

                h1 {
                    margin-top: 20vh;
                    margin-bottom: 1vh;
                    font-size: 80px;
                    line-height: 100px;
                    text-align: center;
                }

                .result-page {
                    background-image: linear-gradient(to right,#8DC2FD,#5997FE);
                    position: absolute;
                    width: 100%;
                    height: fit-content;
                    z-index: -1;
                    margin-top: 40vh;
                    overflow-x: hidden;
                }

                table {
                    border-collapse: collapse;
                    margin: 5vh 25%;
                }
                table td, table th {
                    border: 5px solid #FFFFFF;
                    width: 20vw;
                    height: 5vh;
                    font-family: Muli;
                    font-style: normal;
                    font-size: 28px;
                    line-height: 35px;
                    text-align: center;
                }
                table tr:first-child th {
                    border-top: 0;
                }
                table tr:last-child td {
                    border-bottom: 0;
                }
                table tr td:first-child,
                table tr th:first-child {
                    border-left: 0;
                }
                table tr td:last-child,
                table tr th:last-child {
                    border-right: 0;
                }
                td {
                    text-align: center;
                }

                .input-page-container {
                    width: 100vw;
                    height: 108vh;
                    background-image: linear-gradient(to right, #8DC2FD, #5997FE);
                }

                .bg-top-white-wave {
                    position: relative;
                    text-align: center;
                }

                .bg-top-white-wave img {
                    width: 100vw;
                }
        
                .title-input {
                    position: absolute;
                    top: 8vh;
                    right: 14vw;
                    font-family: Muli;
                    font-size: 4.5vw;
                    font-weight: bold;
                }

                .man-sitting-container {
                    position: absolute;
                    left: 3vw;
                    top: 14vh;
                }

                .man-sitting-container img {
                    width: 38vw;
                }

                .wage-1, .wage-2 {
                    width: 20%;
                    display: inline-block;
                }

                p {
                    margin: 0;
                }

                .wage-to {
                    display: inline-block;
                    margin-left: 1vw;
                    margin-right: 1vw;
                    font-family: Muli;
                    font-size: 1em;
                }

                a { 
                    text-decoration: none;
                    width: 250px;
                    position: absolute;
                    height: 90px;
                    background: #FAD259;
                    box-shadow: 0px 4px 4px rgba(0,0,0,0.25);
                    border-radius: 50px;
                    font-family: 'Muli',sans-serif;
                    font-style: normal;
                    font-weight: bold;
                    font-size: 30px;
                    line-height: 90px;
                    text-align: center;
                    color: #333752;
                    cursor: pointer;
                    left: 15vw;
                }
    
                a:hover {
                    background: #D3A03B;
                }
                
                `}
                </style>
            </div>
        </Provider>
    );
};  

export default withFooter(InputPage);