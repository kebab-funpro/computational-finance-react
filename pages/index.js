import React from 'react';
import { useEffect } from 'react';
import $ from 'jquery';
import withFooter from '../util/footer';
import Link from 'next/link';

const Index = () => {

    useEffect(() => {
        $("#explore").click(function() {
            $('html,body').animate({
                scrollTop: $("#what-why-how").offset().top},
                'slow');
        });
        $('body').css({
            overflowX: 'hidden',
            margin: 0
        });
    })

        return(
            <div>
            <div className="landing-page-head">
                <div className="landing-page-visual">
                    <img id="bg-landing-page-visual" src="../static/image/bg-people.png"/>
                </div>
                <div className="landing-page-about">
                    <div>
                        <h2>Job Search</h2>
                        <p>
                            We are a group of very delicious kebabs and we aspire to make the world a better place with functional programming.
                        </p>
                        <a id='explore'>EXPLORE</a>
                    </div>   
                </div>
            </div>
            <div>
                <img src="../static/image/blue-wave.png" id="wave"/>
            </div>
            <div className="landing-page-description">
                
                <div id="what-why-how">
                    <div className="card-container">
                        <div className="card" style={{marginLeft: '50px'}}>
                            <h3>What?</h3>
                            <hr></hr>
                            <p>This is a financial program to find out whether at the end of the week you should quit your job or not.</p>
                        </div>
                        <div className="card">
                            <h3>Why?</h3>
                            <hr></hr>
                            <p>It is often important for policy makers to consider the rate of labor decision to i dont know what to write.</p>
                        </div>
                        <div className="card" style={{marginRight: '50px'}}>
                            <h3>How?</h3>
                            <hr></hr>
                            <p>We use the probability of getting hired, getting fired, weekly discount, wage, unemployment benefit, psychic satisfaction.</p>
                        </div>
                    </div>
                    <Link href="/input">
                        <a>FIND OUT</a>
                    </Link>
                </div>
            </div>
            <style jsx>{`
            @import url('https://fonts.googleapis.com/css?family=Muli&display=swap');

            html, body {
                overflow-x: hidden !important;
            }

            html {
                position: relative;
                min-width: 1024px;
                min-height: 768px;
                height: 100%;
            }

            body {
                margin: 0 !important;
            }

            p, h2, h3 {
                font-family: Muli;
                font-style: normal;
            }

            a {
                text-decoration: none;
                width: 250px;
                height: 90px;
                background: #FAD259;
                box-shadow: 0px 4px 4px rgba(0,0,0,0.25);
                border-radius: 50px;
                font-family: 'Muli',sans-serif;
                font-style: normal;
                font-weight: bold;
                font-size: 30px;
                line-height: 90px;
                text-align: center;
                color: #333752;
                cursor: pointer;
            }

            a:hover {
                background: #D3A03B;
            }

            .landing-page-head {
                display: flex;
            }

            .landing-page-visual {
                position: absolute;
                width: 69vw;
                left: 20px;
                top: 25px;
                overflow-x: hidden;
                display: contents;
            }

            #bg-landing-page-visual {
                width: 55vw;
            }

            .landing-page-about {
                min-width: 50%;
                max-height: 50%;
                position: relative;
            }

            .landing-page-about > div {
                position: absolute;
                margin-top: 20%;
                margin-left: 25%;
            }

            .landing-page-about h2 {
                position: relative;
                margin: 0;
                font-weight: bold;
                font-size: 70px;
                line-height: 100px;
                color: #000000;
            }

            .landing-page-about p {
                position: absolute;
                width: 100%;
                height: 120px;
                margin-left: 0%;
                font-weight: normal;
                font-size: 18px;
                line-height: 25px;
                text-align: center;
                color: #000000;
            }

            .landing-page-about a {
                position: absolute;
                margin-top: 120px;
                margin-left: 15%;
            }

            #wave {
                overflow: hidden;
                width: 102vw;
                height: 155.54px;
                left: -9px;
            }

            .landing-page-description #what-why-how {
                background: linear-gradient(180deg,#8DC2FD 0%,#5997FE 100%);
                position: relative;
                width: 100%;
                height: 847px;
                margin-top: -6px;
            }

            .landing-page-description #what-why-how a {
                position: absolute;
                left: 42vw;
                top: 600px;
            }

            .card-container {
                position: absolute;
                display: flex;
                width: 100%;
                height: 80%;
            }

            .card {
                position: relative;
                width: 33%;
                height: 60%;
                margin: 80px 20px auto 20px;
                background: #FFFFFF;
                box-shadow: 4px 4px 4px rgba(0, 0, 0, 0.25);
                border-radius: 50px;
            }

            .card h3 {
                position: absolute;
                width: 100%;
                height: 53px;
                font-weight: bold;
                font-size: 42px;
                line-height: 53px;
                color: #333752;
                text-align: center;
            }

            .card hr {
                margin-top: 120px;
                width: 80%;
            }

            .card p {
                position: absolute;
                width: 86%;
                margin: 1em 10px 0 25px;
                font-size: 22px;
                line-height: 35px;
                text-align: center;
                color: #000000;
            }
        `}</style>
        </div>
        )
    
}

export default withFooter(Index);