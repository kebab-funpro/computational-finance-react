const withFooter = (Page) => {
    const PageWithFooter = (props) => (
        <div className="page-container">
            <Page {...props}/>
            <div className="footer-container">
                <h2>Made with &hearts; by <a href="https://gitlab.com/kebab-funpro">Kebab Group</a></h2>
            </div>
            <style jsx>{`
            @import url('https://fonts.googleapis.com/css?family=Muli&display=swap');

            .footer-container {
                background-color: #FAD259;
                padding-top: 2em;
                padding-bottom: 2em;
                font-family: Muli;
            }

            h2 {
                margin: 0;
                text-align: right;
                padding-right: 2em;
            }

            `}</style>
        </div>
    );
    return PageWithFooter;
}

export default withFooter;