﻿# Kebab Job Search - Frontend
﻿
﻿Link deployment: https://kebab-computational-finance.herokuapp.com

## Cara Menjalankan Proyek Secara Lokal

1. Clone repo ini dengan menjalankan `git clone https://gitlab.com/kebab-funpro/computational-finance-react.git`.
2. Install dependensi dengan menjalankan `npm install` di terminal.
3. Jalankan proyek secara lokal dengan `npm run dev` di terminal.
4. Buka `http://localhost:3000/` pada browser.

## Aspek Fungsional
### 1. Pure Component
> ... Pure components in React behave in the exact same way; they receive some props and based on those props, return a component. A pure component avoids any use of state. [source](https://dev.to/keevcodes/pure-functions-in-react-2o7n)

Pada halaman input, terdapat form yang terdiri dari beberapa komponen `Field` yang memiliki tag `label` dan `input`. Agar komponen dapat digunakan berkali-kali, maka dibuat sebuah *pure component* yang menerima beberapa *props* sebagai parameter dan menghasilkan sebuah komponen .

Komponen `renderField` berikut adalah sebuah *pure component* karena hanya menerima *props* seperti `input`, `label`, dan `type` untuk diubah menjadi sebuah komponen, serta tidak memiliki *state*.

File: `pages/input.js`
```
const renderField = ({ input, label, type, meta: { touched, error, warning } }) => (
    <div>
        <div className="control">
        <label className="field">{label}</label>
            <div className="input-container">
                <input className="input" {...input} type={type}/>
                {touched && ((error && <span style={{float:'left'}}>{error}</span>) || (warning && <span>{warning}</span>))}
            </div>
        </div>
    </div>
);
```

yang kemudian digunakan di dalam `form` sebagai berikut:
```
<form onSubmit={handleSubmit}>
    <Field name="unemploymentbenefit" component={renderField} type="number" label="Unemployment Benefit (u)"/>
    <Field name="leisure" component={renderField} type="number" label="Leisure (v)"/>
    ....
</form>
```
### 2. Higher Order Component
> Higher order components usually take a component and optional arguments as input and return an enhanced component of the input component. [source](https://www.robinwieruch.de/react-higher-order-components)

Di setiap halaman, terdapat footer yang disertakan dengan menggunakan *higher order component*. HOC yang dibuat dalam proyek ini adalah komponen `withFooter`. Komponen ini menerima komponen lain sebagai parameter, dalam hal ini sebuah komponen halaman. Kemudian, ditambahkan `div` yang akan berperan sebagai footer di bawah halaman tersebut. Sehingga, komponen yang menjadi hasil akhir adalah komponen halaman yang sudah ditambahkan footer.

File: `util/footer.js`
```
const withFooter = (Page) => {
    const PageWithFooter = (props) => (
        <div className="page-container">
            <Page {...props}/>
            <div className="footer-container">
                <h2>Made with &hearts; by <a href="https://gitlab.com/kebab-funpro">Kebab Group</a></h2>
            </div>
        </div>
    );
}
```
yang kemudian digunakan sebagai berikut pada file `pages/index.js`:
```
...
export default withFooter(Index);
```
### 3. Higher Order Function
> Higher-order functions in Javascript are functions that take other functions as arguments and return another function. Examples of the higher-order functions are the; .map, .filter, etc. [source](https://dev.to/ogwurujohnson/introduction-to-higher-order-components-hocs-in-react-273f#:~:targetText=Higher%2Dorder%20functions%20in%20Javascript,filter%20%2C%20etc.)

Pada pengolahan input yang diterima API, digunakan penggunaan .map() yang merupakan higher order function:
```
data.map((dato, rowIdx) => {
    var row = tableRef.insertRow();
    dato.map((val, index) => {
        var cell = row.insertCell();
        Object.assign(cell.style,{
            textAlign: 'center',
            border: '5px solid #FFFFFF',
            width: '20vw',
            height: '5vh',
            fontFamily: 'Muli',
            fontStyle: 'normal',
            fontSize: '28px',
            lineHeight: '35px'
        });
        
        if(rowIdx == data.length -1) cell.style.borderBottom = 0;

        if(index == 0) cell.style.borderLeft = 0;

        if(index == 2) cell.style.borderRight = 0;

        cell.appendChild(document.createTextNode(addCell(index, val)));
        
    })
})
```

### 4. Currying
> Currying is a process in functional programming in which we can transform a function with multiple arguments into a sequence of nesting functions. It returns a new function that expects the next argument inline. [source](https://blog.bitsrc.io/understanding-currying-in-javascript-ceb2188c339)

Untuk melakukan validasi form, digunakan fungsi currying yang akan mengecek apakah data yang dimasukan sudah sesuai sebelum form disubmit. Terdapat dua jenis input, yaitu untuk tipe `integer` dan tipe `decimal`. Tipe `integer` adalah input untuk dua field Wage(w), Unemployment Benefit(u), dan Leisure(v) di form. Tipe `decimal` adalah input untuk field Probability of Getting Hired(p), Probability of Getting Fired(q), dan Weekly Discount Rate. Input untuk tipe `integer` harus merupakan sebuah integer bernilai lebih dari 0. Input untuk tipe `decimal` harus merupakan bilangan desimal yang bernilai 1 atau kurang dari 1.

Fungsi currying akan membuat dua validator berbeda untuk setiap tipe. 

File: `pages/input.js`

```
function validate_curry(type) {
    return (value) => {
        return type === "integer" ? value != '' && value >= 1 : value != '' && value <= 1;
    }
}

const validateInteger = validate_curry("integer");
const validateDecimal = validate_curry("decimal");
```
yang kemudian digunakan pada fungsi `validate` milik form sebagai berikut:
```
const validate = val => {
    const errors = {};
    if (!validateInteger(val.unemploymentbenefit)) {
        errors.unemploymentbenefit = 'Input is not valid';
    }
    ....
    if (!validateDecimal(val.weeklydiscountrate)) {
        errors.weeklydiscountrate = 'Input is not valid';
    }
    return errors;
};

InputForm = reduxForm({
    form: 'jobSearch',
    validate,
})(InputForm);
```